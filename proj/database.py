import time
from typing import Iterator, List, Optional, Tuple, Union

import rethinkdb as r
from flask import Flask, g

from proj.data_objects import (Ability, AbilityID, AbilityType, AttackEvent,
                               Card, CardID, CardType, EventType, Game,
                               GameCancelEvent, GameEvent, GameID,
                               GameOverEvent, GameStatus, Player, PlayerID)
from proj.exceptions import DataNotFound, InternalError, InvalidUsage
from proj.game import generate_game_state
from proj.utils import generate_token

r.set_loop_type("gevent")


class Database:
    """Manages the database backend of the api."""

    def __init__(self, app: Flask = None):

        #: This lets us use the database object outside an app context
        self._db = None

        self.app = app

        if app is not None:
            self.init_app(app)

    def init_app(self, app: Flask):
        self.app = app
        app.teardown_appcontext(self.stop_db)

    def manual_get_db(self):
        """Used for making a database connection when we're outside of an app context."""
        if self._db is None:
            self._db = self.connect_db()

    def manual_stop_db(self):
        """Used for closing a database connection made outside an app context."""
        if self._db is not None:
            self._db.close()
            self._db = None

    def connect_db(self):
        """Generates a database connection.

        :returns: The database connection.
        """
        return r.connect(host=self.app.config["RDB_HOST"],
                         port=self.app.config["RDB_PORT"],
                         db=self.app.config["RDB_NAME"])

    def get_db(self):
        """Get the database connection, if there isn't a connection, make it.

        :raises InternalError: If the database connection could not be made.
        """
        if self._db is not None:
            return self._db

        if "db" in g:
            return g.db

        try:
            g.db = self.connect_db()
            return g.db
        except r.RqlDriverError as e:
            self.app.logger.error(f"Failed to make a database connection: {e}")
            raise InternalError("Failed to make a database connection")

    @staticmethod
    def stop_db(_):
        """Helper method to close the database connection."""
        db = g.pop("db", None)
        if db is not None:
            db.close()

    def generate_player(self) -> Player:
        """Generates a player, adding the player to the database.

        :returns: The generated player object.
        """
        db = self.get_db()
        uuid = r.uuid().run(db)
        token = generate_token()

        player = Player(id=uuid, token=token)

        r.table("players").insert(player.to_dict()).run(db)
        return player

    def get_player(self, player_id: PlayerID) -> Player:
        """Retrieve a player from the database.

        :raises DataNotFound: If no player was found with the given id.
        :returns: The fetched player object.
        """
        db = self.get_db()
        player = r.table("players").get(player_id).run(db)

        if player is None:
            raise DataNotFound(f"No player with id: '{player_id}' found.", player_id)

        return Player.from_dict(player)

    def confirm_player(self, player_id: PlayerID, token: str) -> bool:
        """Check if a player id and token match up.

        :returns: True if the token is correct, False otherwise.
        """
        player = self.get_player(player_id)

        # not the most secure but ¯\_(ツ)_/¯
        return player.token == token

    def update_player(self, player: Player):
        """Update a player in the database."""
        db = self.get_db()
        r.table("players").get(player.id).replace(player.to_dict()).run(db)

    def generate_game(self, initial_player_id: PlayerID) -> Tuple[Player, Game]:
        """Generate a new game object, adding it to the database and updating the initial player to be in the game.

        Games must have an initial player to start with.

        :raises InvalidUsage: If the given player is already in a game.
        :returns: A tuple of the updated player and the new or updated game objects.
        """
        db = self.get_db()
        uuid = r.uuid().run(db)

        player = self.get_player(initial_player_id)

        if player.game_id is not None:
            raise InvalidUsage("Player is already in a game", "already_in_game")

        player = player.update(game_id=uuid)

        game = Game(id=uuid, status=GameStatus.waiting, gamestate=generate_game_state(),
                    player1_id=initial_player_id)

        r.table("games").insert(game.to_dict()).run(db)
        r.table("players").get(player.id).replace(player.to_dict()).run(db)
        return player, game

    def get_game(self, game_id: GameID) -> Game:
        """Retrieve a game from the database.

        :raises DataNotFound: If no game was found with the given id.
        :returns: The fetched game object.
        """
        db = self.get_db()
        game = r.table("games").get(game_id).run(db)

        if game is None:
            raise DataNotFound(f"No game with id: '{game_id}' found.", game_id)

        return Game.from_dict(game)

    def add_game_player(self, game_id: GameID, player_id: PlayerID) -> Tuple[Player, Game]:
        """Add a player to a game.

        As games have atleast one player to start with, this marks the game as ready.

        :raises InvalidUsage: If the game is not in the waiting state, the given player is already in a game,
                              or if the player is already in the given game.
        :returns: A tuple of the updated player and the updated game objects.
        """
        db = self.get_db()

        player = self.get_player(player_id)
        game = self.get_game(game_id)

        if game.status is not GameStatus.waiting:
            raise InvalidUsage(f"Game was not in waiting state, but was {game.status}", "game_not_in_waiting_state")

        if player.game_id is not None:
            raise InvalidUsage(f"Player is already in a game", "player_already_playing")

        player = player.update(game_id=game.id)
        r.table("players").get(player_id).update({"game_id": game.id}).run(db)

        assert game.player1_id is not None  # noqa

        # ensure you can't join a game twice
        if game.player1_id == player_id:
            raise InvalidUsage(f"player with id: {player_id} has already joined this game.", "player_already_in_game")

        game = game.update(player2_id=player.id, status=GameStatus.ready)
        self.update_game(game)

        return player, game

    def start_game(self, game_id: GameID) -> Game:
        """Starts a game, setting it's start time and changing it's status to ongoing.

        :raises InvalidUsage: If the game is not ready to start yet, or not all players are connected.
        :returns: The updated game object.
        """
        game = self.get_game(game_id)

        if game.status is not GameStatus.ready:
            raise InvalidUsage("Game is not ready to start yet.", "game_not_ready")

        player_1 = self.get_player(game.player1_id)
        player_2 = self.get_player(game.player2_id)

        if not (player_1.socket_connected and player_2.socket_connected):
            raise InvalidUsage("Not all players are connected yet", "players_not_connected")

        game = game.update(status=GameStatus.ongoing, start_time=int(time.time()))
        self.update_game(game)
        self.publish_game_event(EventType.game_start, game.id, None)
        return game

    def maybe_start_game(self, game_id: GameID) -> Game:
        """Starts a game if the game is ready to start.

        :returns: The possibly updated game object, the state will become
            :class:`GameStatus.ongoing` if the game has started.
        """
        try:
            return self.start_game(game_id)
        except InvalidUsage as e:
            # if the error wasn't from start_game, reraise
            if e.error_code not in ("game_not_ready", "players_not_connected"):
                raise e from None

    def update_game(self, game: Game):
        """Update a game in the database."""
        db = self.get_db()

        game_dict = game.to_dict()

        r.table("games").get(game.id).replace(game_dict).run(db)

    def get_waiting_games(self) -> List[Game]:
        """Gets all games that are in the waiting state.

        :returns: A list of game objects that are waiting to be filled.
        """
        db = self.get_db()

        rows = r.table("games").filter(r.row["status"] == "waiting").run(db)
        return [Game.from_dict(row) for row in rows]

    def join_or_spawn_game(self, player_id: PlayerID) -> Tuple[Player, Game]:
        """Gets a game to play.

        If there are any waiting games: join one of the waiting games.
        Otherwise if there are no waiting games: generate a new game.

        :returns: A tuple of the updated player and the new or updated game objects.
        """
        db = self.get_db()

        waiting_games = r.table("games").filter(
            (r.row["status"] == "waiting") & (r.row["player1_id"] != player_id)).run(db)

        # if there are any waiting games, join the first one
        for game in map(Game.from_dict, waiting_games):
            return self.add_game_player(game.id, player_id)

        # otherwise, make a new game
        player, game = self.generate_game(player_id)
        return player, game

    def new_card(self, name: str, type: CardType, description: str, abilities: List[AbilityID]) -> Card:
        """Insert a card to the database.

        :returns: The generated card object.
        """
        db = self.get_db()
        uuid = r.uuid().run(db)

        card = Card(id=uuid, name=name, description=description, type=type, abilities=abilities)

        r.table("cards").insert(card.to_dict()).run(db)
        return card

    def get_card(self, *, card_id: Optional[CardID] = None, card_name: Optional[str] = None) -> Card:
        """Get a card from the database, either by id or by name.

        If both parameters are passed, the parameter :card_name: is ignored.

        :raises TypeError: If no parameters to get by are passed.
        :raises DataNotFound: If no card with the given name or id could be found.
        :returns: The retrieved card object.
        """
        db = self.get_db()

        if card_id is not None:
            card = r.table("cards").get(card_id).run(db)
            if card is None:
                raise DataNotFound(f"No card with id: '{card_id}' found.", card_id)

        elif card_name is not None:
            card = list(r.table("cards").filter({"name": card_name}).run(db))
            card = card[0] if card else None
            if card is None:
                raise DataNotFound(f"No card with name: '{card_name}' found.", card_name)
        else:
            raise TypeError("At least one parameter of :card_id: or :card_name: must be provided.")

        return Card.from_dict(card)

    def get_random_cards(self, amount: int) -> List[Card]:
        """Retrieves random cards from the database of cards.

        :returns: The retrieved list of random cards.
        """
        db = self.get_db()

        cards = r.table("cards").sample(amount).run(db)

        return [Card.from_dict(card) for card in cards]

    def update_card(self, card: Card):
        """Update a player in the database."""
        db = self.get_db()

        r.table("cards").get(card.id).replace(card.to_dict()).run(db)

    def publish_game_event(self, typ: EventType, game_id: GameID,
                           data: Optional[Union[GameOverEvent, GameCancelEvent, AttackEvent]]) -> GameEvent:
        """Publish a game event to the database.

        :returns: The published game event.
        """
        db = self.get_db()
        uuid = r.uuid().run(db)

        event = GameEvent(id=uuid, type=typ, game_id=game_id, data=data)

        r.table("game_events").insert(event.to_dict()).run(db)
        return event

    def event_stream(self, game_id: GameID) -> Iterator[GameEvent]:
        """Get an iterator of all events concerning a game.

        :returns: An iterator that yields each game event as they happen.
        """
        db = self.get_db()

        # ensure the game exists
        self.get_game(game_id)

        events = (r.table("game_events")
                  .filter(r.row["game_id"] == game_id)
                  .changes()
                  .filter(r.row["old_val"].eq(None))
                  .run(db))

        for event in events:
            yield GameEvent.from_dict(event["new_val"])

    def new_ability(self, name: str, type: AbilityType, damage: int) -> Ability:
        """Construct a new ability, adding it to the database.

        :returns: The constructed ability object.
        """
        db = self.get_db()
        uuid = r.uuid().run(db)

        ability = Ability(id=uuid, name=name, type=type, damage=damage)

        r.table("abilities").insert(ability.to_dict()).run(db)
        return ability

    def get_ability(self, *, ability_id: Optional[AbilityID] = None, ability_name: Optional[str] = None) -> Ability:
        """Get a ability from the database, either by id or by name.

        If both parameters are passed, the parameter :ability_name: is ignored.

        :raises TypeError: If no parameters to get by are passed.
        :raises DataNotFound: If no ability with the given name is found.
        :returns: The retrieved ability object.
        """
        db = self.get_db()

        if ability_id is not None:
            ability = r.table("abilitys").get(ability_id).run(db)
            if ability is None:
                raise DataNotFound(f"No ability with id: '{ability_id}' found.", ability_id)

        elif ability_name is not None:
            ability = list(r.table("abilitys").filter({"name": ability_name}).run(db))
            ability = ability[0] if ability else None
            if ability is None:
                raise DataNotFound(f"No ability with name: '{ability_name}' found.", ability_name)
        else:
            raise TypeError("At least one parameter of :ability_id: or :ability_name: must be provided.")

        return Ability.from_dict(ability)


def db_setup(app: Flask):
    """Sets up the database and tables."""
    conn = r.connect(host=app.config["RDB_HOST"], port=app.config["RDB_PORT"])
    db_name = app.config["RDB_NAME"]

    try:
        r.db_create(db_name).run(conn)
        r.db(db_name).table_create("players", primary_key="id").run(conn)
        r.db(db_name).table_create("games", primary_key="id").run(conn)
        r.db(db_name).table_create("cards", primary_key="id").run(conn)
        r.db(db_name).table_create("game_events", primary_key="id").run(conn)
        r.db(db_name).table_create("abilities", primary_key="id").run(conn)

        r.db(db_name).table("cards").index_create("name").run(conn)
        r.db(db_name).table("abilities").index_create("name").run(conn)
        app.logger.info("Database setup completed")
    except r.RqlRuntimeError as e:
        app.logger.warn(f"Database and tables is already setup! ({e})")
    finally:
        conn.close()


def db_delete(app: Flask):
    """Delete the database and all data."""
    conn = r.connect(host=app.config["RDB_HOST"], port=app.config["RDB_PORT"])
    db_name = app.config["RDB_NAME"]

    if db_name in r.db_list().run(conn):
        r.db_drop(db_name).run(conn)


def generate_resources(app: Flask):
    """Generate resources for the project."""
    import re
    import os
    import json
    import random
    import requests
    from bs4 import BeautifulSoup

    app.db.manual_get_db()

    # generate abilities
    r = requests.get("http://typemoon.wikia.com/wiki/Noble_Phantasm")
    soup = BeautifulSoup(r.text, "html.parser")

    list_of_phantasms = soup.find(id="List_of_known_Noble_Phantasms").parent.find_next_siblings(class_="wikitable")[0]

    rows = list_of_phantasms.find_all('tr')[1:]
    phantasms = [i.td.text.strip() for i in rows]

    remove_parens = re.compile(r"\(.*\)")

    phantasms = [remove_parens.sub("", i).strip() for i in phantasms]

    def gen_ability(name: str) -> Ability:
        type_ = random.choice(list(AbilityType))
        damage = random.choice((35, 40, 45, 50))

        return app.db.new_ability(name, type_, damage)

    abilities = [gen_ability(phantasm) for phantasm in phantasms]

    # generate cards
    gods_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), "resources", "gods.json")

    def gen_card(name: str, description: str) -> Card:
        type_ = random.choice(list(CardType))
        card_abilities = [i.id for i in random.sample(abilities, 10)]

        return app.db.new_card(name, type_, description, card_abilities)

    with open(gods_file) as f:
        gods = json.load(f)

    cards = [gen_card(god["name"], god["god_of"]) for god in gods]

    app.db.manual_stop_db()

    app.logger.info(f"Generated {len(abilities)} abilities and {len(cards)} cards.")
