import json
import secrets
from typing import Any, Mapping, Tuple, TypeVar

from flask import abort


def generate_token():
    """Generate a token for use in the api."""
    return secrets.token_urlsafe(16)


def ensure_params(d: dict, *names: str):
    """Helper function to ensure parameters from a post request exist and to retrieve them.

    If number of params given is 1, return a single param, otherwise returns a list of params.
    """
    def inner(name):
        if name not in d:
            abort(400, f"Parameter: '{name}' missing")
        return d[name]
    params = [inner(name) for name in names]
    if len(params) == 1:
        return params[0]
    return params


def attribute_bind(obj, attr: str):
    """returns :obj.attr: if :obj: is not None, else None."""
    if obj is None:
        return None
    return getattr(obj, attr)


def send_json(ws, d):
    """Send json to a socket."""
    ws.send(json.dumps(d))


K = TypeVar('K')
V = TypeVar('V')


class ImmutableDict(Mapping[K, V]):
    """An immutable dictionary."""

    def __init__(self, *args, **kwargs):
        self._inner_dict = dict(*args, **kwargs)

    def __getitem__(self, key):
        return self._inner_dict[key]

    def __iter__(self):
        return iter(self._inner_dict)

    def __len__(self):
        return len(self._inner_dict)

    def _immutable_dict_wrapper(self, func, *args, **kwargs) -> Tuple[Any, 'ImmutableDict']:
        """Internal hepler function of the immutable dict."""
        new = self._inner_dict.copy()
        val = func(new, *args, **kwargs)
        return val, type(self)(new)

    def pop(self, *args, **kwargs) -> Tuple[V, 'ImmutableDict']:
        """Pop a key from the dict, returning the popped value and the updated dict."""
        return self._immutable_dict_wrapper(dict.pop, *args, **kwargs)

    def popitem(self) -> Tuple[Tuple[K, V], 'ImmutableDict']:
        """Same as dict.popitem, but returns a tuple of the kv pair and the updated dict."""
        return self._immutable_dict_wrapper(dict.popitem)

    def set(self, key: K, val: V) -> 'ImmutableDict':
        """Set a key of the dict, returning the updated dict."""
        _, new = self._immutable_dict_wrapper(dict.__setitem__, key, val)
        return new

    def update(self, *args, **kwargs) -> 'ImmutableDict':
        _, new = self._immutable_dict_wrapper(dict.update, *args, **kwargs)
        return new

    def __serialise__(self):
        return self._inner_dict

    @classmethod
    def __deserialise__(cls, val: dict) -> 'ImmutableDict':
        return cls(val)
