from proj.routes import cards, games, players, ws

routes = (
    cards.cards,
    games.games,
    players.players
)

socket_routes = (
    ws.ws,
)

# api doc stuff
"""
@api {error} /
@apiName ErrorsDataNotFound
@apiGroup Errors
@apiError {json} DataNotFound
    Represents a resource not being found, contains an explanation of what was not found,
    and the key of the data that was not found.
@apiErrorExample {json} DataNotFound
    HTTP/1.1 404 Not Found
    {
        "message": "No user for id: '2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee' found",
        "key": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee"
    }
"""

"""
@api {error} /
@apiName ErrorsInvalidUsage
@apiGroup Errors
@apiError {json} InvalidUsage
    Represents the api being used incorrectly, contains a description of the error,
    and a code representing the error that happened.
@apiErrorExample {json} InvalidUsage
    HTTP/1.1 400 Bad Request
    {
        "message": "Incorrect token",
        "error_code": "token_incorrect"
    }
"""

"""
@api {error} /
@apiName ErrorsInvalidUsageCodes
@apiGroup Errors
@apiDescription The codes for the errors that can be in the error_code field of the InvalidUsage error.
@apiError already_in_game Player is already in a game.
@apiError game_not_in_waiting_state The game is not in the waiting state.
@apiError player_already_playing The player is already in a game.
@apiError game_not_ready Attempting to start a game that is not ready.
@apiError player_not_connected Game cannot start as not all players are connected.
@apiError token_incorrect A passed token was not correct.
@apiError game_not_running A game is not in the running state.
@apiError cannot_attack_now It is not possible to attack currently.
@apiError not_valid_state The game is not in a valid state to play.
@apiError not_valid_player Attempt to connect to a ws for a game the player is not a player in.
"""
