from flask import Blueprint, current_app, jsonify

from proj.data_objects import Ability

abilities = Blueprint("abilities", __name__, url_prefix="/abilities")


@abilities.route("/<uuid:id>")
def get_ability_by_id(id):
    """Retrieve a ability by id.

    @api {get} /abilities/:id
    @apiName GetAbilityByID
    @apiGroup Abilities

    @apiParam {String} id ID of ability to get.

    @apiSuccessExample {json} Success
        HTTP/1.1 200 OK
        {
            "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
            "name": "Agni Gandiva",
            "type": "magic",
            "damage": 20
        }
    """
    ability = Ability.fetch(str(id), current_app.db)
    return jsonify(ability.to_dict())


@abilities.route("/name/<name>")
def get_ability_by_name(name):
    """Retrieve a ability by name.

    @api {get} /abilities/name/:name
    @apiName GetAbilityByName
    @apiGroup Abilities

    @apiParam {String} name Name of ability to get.

    @apiSuccessExample {json} Success
        HTTP/1.1 200 OK
        {
            "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
            "name": "Agni Gandiva",
            "type": "magic",
            "damage": 20
        }
    """
    ability = current_app.db.get_ability(ability_name=name)
    return jsonify(ability.to_dict())
