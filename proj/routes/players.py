from flask import Blueprint, current_app, jsonify

players = Blueprint("players", __name__, url_prefix="/players")


@players.route("/new")
def new_player():
    """Generate a player to use in playing a game.

    @api {get} /players/new Generate a player to use in playing a game
    @apiName NewPlayer
    @apiGroup Players

    @apiSuccessExample {json} Success
        HTTP/1.1 200 OK
        {
            "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
            "token": "qADBiiggxUCiHI_6TeQtwA",
            "game_id": null,
            "socket_connected": false
        }
    """
    player = current_app.db.generate_player()
    return jsonify(player.to_dict())
