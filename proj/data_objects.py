from enum import Enum
from typing import List, Mapping, Optional, Union

import rethinkdb as r
from dataclasses import asdict, dataclass, replace

from proj.exceptions import DataNotFound
from proj.utils import ImmutableDict


class DataclassSerialiserMixin:
    """Allows for dataclasses to be serialised and unserialised to dicts.

    For Serialising
    ===============
    - Fields that have the __serialise__ dunder method will have that called and used as the value.
    - Fields that do not will be used as values in the dictionary as-is.


    For Unserialising
    =================
    - Fields that have a type that implements __unserialise__ will have
      Type.__unserialise__(value) called with the value from the dict.
    - This doesn't work correctly with nested types as of yet,
      if the feature is needed it can be added however.

    """

    def __serialise__(self):
        def maybe_serialise(val):
            if hasattr(val, "__serialise__"):
                val = val.__serialise__()

            if isinstance(val, (list, tuple)):
                val = type(val)(map(maybe_serialise, val))

            if isinstance(val, Mapping):
                val = type(val)({k: maybe_serialise(v) for k, v in val.items()})

            return val

        return {key: maybe_serialise(val) for key, val in asdict(self).items()}

    @classmethod
    def __deserialise__(cls, dataclass):
        def maybe_deserialise(typ, val):
            if val is None:
                return None

            if hasattr(typ, "__deserialise__"):
                val = typ.__deserialise__(val)

            return val

        return cls(**{
            key: maybe_deserialise(field.type, dataclass[key]) for key, field
            in cls.__dataclass_fields__.items()
        })

    def to_dict(self):
        """Serialise this dataclass into a dict."""
        return self.__serialise__()

    @classmethod
    def from_dict(cls, d):
        """Deserialise a dict into this dataclass."""
        return cls.__deserialise__(d)


class DataclassUpdaterMixin:
    """Allows for dataclasses to be updated with new values, returning the updated object."""
    update = replace


class DataclassRDBMixin(DataclassSerialiserMixin):
    """Mixin for making a serialisable dataclass able to be fetched and updated in the database.

    For example: `data = DataClass.fetch("1234", current_app.db)`

    Requires attributes '_table' and '_primkey' to be set on the class.
    """

    @classmethod
    def fetch(cls, id: str, db: "Database"):
        """Fetch the data for the given id from the database.

        :raises DataNotFound: Data corresponding to given id was not found.
        """
        db_conn = db.get_db()
        data = r.table(cls._table).get(id).run(db_conn)

        if data is None:
            raise DataNotFound(f"No {cls.__name__} with {cls._primkey}: '{id}' found", id)

        return cls.from_dict(data)

    def update_on_db(self, db: "Database"):
        """Update this data in the database."""
        db_conn = db.get_db()
        r.table(self._table).get(getattr(self, self._primkey)).replace(self.to_dict()).run(db_conn)


class SerialisableEnum(Enum):
    def __serialise__(self):
        return self.value

    @classmethod
    def __deserialise__(cls, val):
        return cls(val)


# type aliases
GameID, PlayerID, CardID, AbilityID, EventID = str, str, str, str, str


@dataclass(frozen=True)
class Player(DataclassRDBMixin, DataclassUpdaterMixin):
    _table = "players"
    _primkey = "id"

    id: PlayerID
    token: str
    game_id: Optional[GameID] = None
    socket_connected: bool = False


class PlayerType(SerialisableEnum):
    player_1 = "player_1"
    player_2 = "player_2"


class GameStatus(SerialisableEnum):
    waiting = "waiting"
    ready = "ready"
    ongoing = "ongoing"
    finished = "finished"
    cancelled = "cancelled"


@dataclass(frozen=True)
class GameState(DataclassRDBMixin, DataclassUpdaterMixin):
    player1_cards: ImmutableDict[CardID, int]
    player2_cards: ImmutableDict[CardID, int]
    current_turn: PlayerType = PlayerType.player_1


@dataclass(frozen=True)
class Game(DataclassRDBMixin, DataclassUpdaterMixin):
    _table = "games"
    _primkey = "id"

    id: GameID
    status: GameStatus
    gamestate: GameState

    player1_id: PlayerID
    player2_id: Optional[PlayerID] = None
    start_time: Optional[int] = None
    last_update: Optional[int] = None

    def is_ready(self) -> bool:
        """Is this game ready to start?"""
        return (self.player1_id or self.player2_id) is not None


class CardType(SerialisableEnum):
    saber = "saber"
    archer = "archer"
    lancer = "lancer"


@dataclass(frozen=True)
class Card(DataclassRDBMixin, DataclassUpdaterMixin):
    _table = "cards"
    _primkey = "id"

    id: CardID
    name: str
    description: str

    type: CardType
    abilities: List[AbilityID]


class EventType(SerialisableEnum):
    game_ready = "game_ready"
    game_start = "game_start"
    game_end = "game_end"
    game_cancel = "game_cancel"
    attack = "attack"


@dataclass(frozen=True)
class GameEvent(DataclassRDBMixin, DataclassUpdaterMixin):
    _table = "events"
    _primkey = "id"

    id: EventID
    type: EventType
    game_id: GameID
    data: Optional[Union['GameOverEvent', 'GameCancelEvent', 'AttackEvent']]

    @classmethod
    def __deserialise__(cls, val):
        event_type = EventType.__deserialise__(val["type"])

        # deserialise the data payload from the adjacent tag
        val["data"] = {
            EventType.attack: AttackEvent,
            EventType.game_cancel: GameCancelEvent,
            EventType.game_end: GameOverEvent,
        }.get(event_type, lambda x: x)(val["data"])

        return super().__deserialise__(val)


class AbilityType(SerialisableEnum):
    ranged = "ranged"
    melee = "melee"
    magic = "magic"


@dataclass(frozen=True)
class Ability(DataclassRDBMixin, DataclassUpdaterMixin):
    _table = "abilities"
    _primkey = "id"

    id: AbilityID
    name: str

    type: AbilityType
    damage: int


@dataclass(frozen=True)
class AttackEvent(DataclassSerialiserMixin, DataclassUpdaterMixin):
    attacking_player: PlayerType
    attacking_card_id: CardID
    attacking_ability_id: AbilityID
    attacked_card_id: CardID

    new_state: GameState


@dataclass(frozen=True)
class GameOverEvent(DataclassSerialiserMixin, DataclassUpdaterMixin):
    winner_id: PlayerID


@dataclass(frozen=True)
class GameCancelEvent(DataclassSerialiserMixin, DataclassUpdaterMixin):
    reason: str
